function MD_info = Extract_MD_info(Trial,Processing)

% Initialisation
MD_info.filt_markers = [];
MD_info.filt_method  = [];
MD_info.filt_param   = [];
MD_info.filt_order   = [];

MD_info.gap_markers = [];
MD_info.gap_method  = [];
MD_info.gap_param   = [];
MD_info.gap_size    = [];

% Smooth/Filt info (1 value per marker)
for i = 1:size(Trial.Marker,2)
    if ~strcmp(Trial.Marker(i).Processing.smooth,'none') 
        MD_info.filt_markers = [MD_info.filt_markers;{Trial.Marker(i).label}];
        MD_info.filt_method  = [MD_info.filt_method;{Trial.Marker(i).Processing.smooth}]; 
        MD_info.filt_param   = [MD_info.filt_param;{Trial.Marker(i).Processing.smooth_param}]; 
        MD_info.filt_order   = [MD_info.filt_order;{Trial.Marker(i).Processing.smooth_order}]; 
    end
end
clear i

% Gap fill info (1 or more values per marker)
for i = 1:size(Trial.Marker,2)  
    for j = 1:size(Trial.Marker(i).Processing.Gap,2)
        MD_info.gap_markers = [MD_info.gap_markers;{Trial.Marker(i).label}];
        MD_info.gap_method  = [MD_info.gap_method;{Trial.Marker(i).Processing.Gap(j).reconstruction}];
        if isempty(Processing.Marker.fmethod.parameter)
            MD_info.gap_param = [MD_info.gap_param;{'No parameter'}]; 
        else
            MD_info.gap_param = [MD_info.gap_param;Processing.Marker.fmethod.parameter]; 
        end
        MD_info.gap_size = [MD_info.gap_size;size(Trial.Marker(i).Trajectory.Gap(j).frames,2)]; % gap size
    end
end
clear i