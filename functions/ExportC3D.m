% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : December 2021
% -------------------------------------------------------------------------
% Description  : This routine aims to export C3D files with updated data.
% Inputs       : To be defined
% Outputs      : To be defined
% -------------------------------------------------------------------------
% Dependencies : - Biomechanical Toolkit (BTK): https://github.com/Biomechanical-ToolKit/BTKCore
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function ExportC3D(Patient_ID,Session_ID,Session_date,Session_protocol,Folder,Trial,Processing,Units,event,marker,emg,force,grf,rename_files,MD_removed,MD_removed_sublist,MD_info)

% Set new C3D file
% btkFile = btkNewAcquisition();
% btkSetFrequency(btkFile,Trial.fmarker);
% btkSetFrameNumber(btkFile,Trial.n1);
% btkSetPointsUnit(btkFile,'marker',Units.output);
% btkSetAnalogSampleNumberPerFrame(btkFile,ceil(Trial.fanalog/Trial.fmarker));
btkFile = btkCloneAcquisition(Trial.btk);
btkSetPointsUnit(btkFile,'marker',Units.output);
% CLEAR MD ACCORDING TO PROCESS PERFORMED
if grf || force || emg
    btkClearAnalogs(btkFile);
end
btkClearAnalysis(btkFile);
if marker
    btkClearPoints(btkFile);
end
if grf
    btkRemoveMetaData(btkFile,'FORCE_PLATFORM');
end
% CLEAR MD ACCORDING TO CHOICE (txt file)
for i = 1:size(MD_removed,1)
    for j = 1:size(MD_removed_sublist,1)
        btkRemoveMetaData(btkFile,MD_removed{i},MD_removed_sublist{j});
    end
end

% Append events
if event == 1
    if ~isempty(Trial.Event)
        for i = 1:size(Trial.Event,2)
            for j = 1:size(Trial.Event(i).value,2)
                Event = Trial.Event(i).value(1,j)/Trial.fmarker;
                btkAppendEvent(btkFile,Trial.Event(i).label,Event,'');
                clear Event;
            end
        end
    end
end

% Append marker trajectories
if marker == 1
    if ~isempty(Trial.Marker)
        for i = 1:size(Trial.Marker,2)
            if ~isempty(Trial.Marker(i).Trajectory.smooth) 
                btkAppendPoint(btkFile,'marker',Trial.Marker(i).label,permute(Trial.Marker(i).Trajectory.smooth,[3,1,2]),zeros(size(permute(Trial.Marker(i).Trajectory.smooth,[3,1,2]),1),1),['Units: ',Trial.Marker(i).Trajectory.units]);
            else
                btkAppendPoint(btkFile,'marker',Trial.Marker(i).label,zeros(Trial.n1,3),zeros(Trial.n1,1),'Units: NA');
            end    
        end
    end
    % add marker filter information in PROCESSING
    if ~strcmp(Trial.Marker(1).Processing.smooth,'none')
        INFO = btkMetaDataInfo('Char', {'Pre-process Toolbox, Moissenet - KLab'});
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILTER_SOFTWARE_Value', INFO);
        btkSetMetaDataDescription(btkFile, 'PROCESSING', 'MARKER_FILTER_SOFTWARE_Value', 'https://gitlab.unige.ch/KLab/preprocessing_toolbox');
        INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd, HH:MM:SS')});
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILTER_PROCESS_TIME_Value', INFO);
        INFO = btkMetaDataInfo('Char', MD_info.filt_markers);
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_NAMES_FILT_Value', INFO);
        INFO = btkMetaDataInfo('Char', MD_info.filt_method);
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILTER_METHOD_Value', INFO);
        INFO = btkMetaDataInfo('Real', cell2mat(MD_info.filt_param));
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILTER_PARAMETER_CutOffFrequency_hz', INFO);
        INFO = btkMetaDataInfo('Real', cell2mat(MD_info.filt_order));
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILTER_PARAMETER_Order', INFO);
    end
    % add marker fill gap information in PROCESSING
    if ~strcmp(Trial.Marker(1).Processing.smooth,'none') && ~isempty(MD_info.gap_markers) 
        INFO = btkMetaDataInfo('Char', {'Pre-process Toolbox, Moissenet - KLab'});
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILLGAP_SOFTWARE_Value', INFO);
        btkSetMetaDataDescription(btkFile, 'PROCESSING', 'MARKER_FILLGAP_SOFTWARE_Value', 'https://gitlab.unige.ch/KLab/preprocessing_toolbox');
        INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd, HH:MM:SS')});
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILLGAP_TIME_Value', INFO);
        INFO = btkMetaDataInfo('Char', MD_info.gap_markers);
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_NAMES_FILLGAP_Value', INFO);
        INFO = btkMetaDataInfo('Char', MD_info.gap_method);
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILLGAP_METHOD_Value', INFO);
        if iscell(MD_info.gap_param)
            INFO = btkMetaDataInfo('Char', MD_info.gap_param);
        else
            INFO = btkMetaDataInfo('Real', MD_info.gap_param);
        end
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILLGAP_PARAMETER_MaxGapFactor', INFO);
        INFO = btkMetaDataInfo('Real', MD_info.gap_size);
        btkAppendMetaData(btkFile, 'PROCESSING', 'MARKER_FILLGAP_GAPSIZE_Value', INFO);
    end
end

% Append EMG signals
if emg == 1
    if ~isempty(Trial.EMG)
        for i = 1:size(Trial.EMG,2)
            if ~isempty(Trial.EMG(i).Signal.smooth)
                btkAppendAnalog(btkFile,Trial.EMG(i).label,permute(Trial.EMG(i).Signal.smooth,[3,1,2]),'EMG signal (mV)');
            else
                btkAppendAnalog(btkFile,Trial.EMG(i).label,zeros(ceil(Trial.n1*Trial.fanalog/Trial.fmarker),1),'EMG signal (mV)');
            end
        end
    end
end

% Append Force signals
if force == 1
    if ~isempty(Trial.Force)
        for i = 1:size(Trial.Force,2)
            if ~isempty(Trial.Force(i).Signal.smooth)
                btkAppendAnalog(btkFile,Trial.Force(i).label,permute(Trial.Force(i).Signal.smooth,[3,1,2]),'Force signal (mV)');
            else
                btkAppendAnalog(btkFile,Trial.Force(i).label,zeros(ceil(Trial.n1*Trial.fanalog/Trial.fmarker),1),'Force signal (mV)');
            end
        end
    end
end

% Append GRF signals
if grf == 1
    if ~isempty(Trial.GRF)
        for i = 1:size(Trial.GRF,2)
            if ~isempty(Trial.GRF(i).Signal.F.smooth)
                F      = permute(Trial.GRF(i).Signal.F.smooth,[3,1,2]);
                F(:,1) = -F(:,1);
                F(:,3) = -F(:,3);
                M      = permute(Trial.GRF(i).Signal.M.smooth,[3,1,2]);
                M(:,1) = -M(:,1);
                M(:,3) = -M(:,3);
                btkAppendForcePlatformType2(btkFile,...
                                            F,...
                                            M,...
                                            Trial.GRF(i).corners',[0 0 0],1);
            else
                btkAppendForcePlatformType2(btkFile,...
                                            zeros(size(Trial.GRF(i).Signal.F.raw)),...
                                            zeros(size(Trial.GRF(i).Signal.M.raw)),...
                                            Trial.GRF(i).corners',[0 0 0],1);
            end
        end
    end
    
    if ~contains(Trial.type,'SB')
        % add GRF process information in PROCESSING (zeroing)
        INFO = btkMetaDataInfo('Char', {'Pre-process Toolbox, Moissenet - KLab'});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_ZERO_SOFTWARE_Value', INFO);
        btkSetMetaDataDescription(btkFile, 'PROCESSING', 'GRF_ZERO_SOFTWARE_Value', 'https://gitlab.unige.ch/KLab/preprocessing_toolbox');
        INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd, HH:MM:SS')});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_ZERO_PROCESS_TIME_Value', INFO);
        INFO = btkMetaDataInfo('Char', {Trial.GRF(1).Processing.zero});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_ZERO_METHOD_Value', INFO);
        if isempty(Processing.GRF.zmethod.parameter)
            INFO = btkMetaDataInfo('Char', {'No parameter'});
        else        
            INFO = btkMetaDataInfo('Real', Processing.GRF.zmethod.parameter);
        end
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_ZERO_PARAMETER_Value', INFO);
    
        % add GRF process information in PROCESSING (filtering)
        INFO = btkMetaDataInfo('Char', {'Pre-process Toolbox, Moissenet - KLab'});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_FILTER_SOFTWARE_Value', INFO);
        btkSetMetaDataDescription(btkFile, 'PROCESSING', 'GRF_FILTER_SOFTWARE_Value', 'https://gitlab.unige.ch/KLab/preprocessing_toolbox');
        INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd, HH:MM:SS')});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_FILTER_PROCESS_TIME_Value', INFO);
        INFO = btkMetaDataInfo('Char', {Trial.GRF(1).Processing.filt});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_FILTER_METHOD_Value', INFO);
        INFO = btkMetaDataInfo('Real', Processing.GRF.fmethod.parameter);
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_FILTER_PARAMETER_ForceThreshold_N', INFO);
    
        % add GRF process information in PROCESSING (smoothing)
        INFO = btkMetaDataInfo('Char', {'Pre-process Toolbox, Moissenet - KLab'});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_SMOOTH_SOFTWARE_Value', INFO);
        btkSetMetaDataDescription(btkFile, 'PROCESSING', 'GRF_SMOOTH_SOFTWARE_Value', 'https://gitlab.unige.ch/KLab/preprocessing_toolbox');
        INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd, HH:MM:SS')});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_SMOOTH_PROCESS_TIME_Value', INFO);
        INFO = btkMetaDataInfo('Char', {Trial.GRF(1).Processing.smooth});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_SMOOTH_METHOD_Value', INFO);
        INFO = btkMetaDataInfo('Real', Processing.GRF.smethod.parameter);
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_SMOOTH_PARAMETER_CutOffFrequency_hz', INFO);
        if contains(Processing.GRF.smethod.type,'butterLow2')
            INFO = btkMetaDataInfo('Real', 2);
        else
            INFO = btkMetaDataInfo('Char', {'NaN'});
        end
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_SMOOTH_PARAMETER_Order', INFO);
    
        % add GRF process information in PROCESSING (resampling)
        INFO = btkMetaDataInfo('Char', {'Pre-process Toolbox, Moissenet - KLab'});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_RESAMPLE_SOFTWARE_Value', INFO);
        btkSetMetaDataDescription(btkFile, 'PROCESSING', 'GRF_RESAMPLE_SOFTWARE_Value', 'https://gitlab.unige.ch/KLab/preprocessing_toolbox');
        INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd, HH:MM:SS')});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_RESAMPLE_PROCESS_TIME_Value', INFO);
        INFO = btkMetaDataInfo('Char', {Trial.GRF(1).Processing.resamp});
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_RESAMPLE_METHOD_Value', INFO);
        if isempty(Processing.GRF.rmethod.parameter)
            INFO = btkMetaDataInfo('Char', {'No parameter'});
        else
            INFO = btkMetaDataInfo('Real', Processing.GRF.rmethod.parameter);
        end
        btkAppendMetaData(btkFile, 'PROCESSING', 'GRF_RESAMPLE_PARAMETER_Value', INFO);
    end
end

% Export C3D file
temp = regexprep(Trial.file,'.c3d','');
if contains(Trial.file,'Calibration_force')
    task = 'CALIB1';
    num = str2num(regexprep(temp,'Calibration_force ',''));
elseif contains(Trial.file,'Elevation_coronal')
    task = 'TASK1';
    num = str2num(regexprep(temp,'Elevation_coronal ',''));
elseif contains(Trial.file,'Elevation_sagittal')
    task = 'TASK2';
    num = str2num(regexprep(temp,'Elevation_sagittal ',''));
elseif contains(Trial.file,'Isometric_left')
    task = 'TASK3';
    num = str2num(regexprep(temp,'Isometric_left ',''));
elseif contains(Trial.file,'Isometric_right')
    task = 'TASK4';
    num = str2num(regexprep(temp,'Isometric_right ',''));
elseif contains(Trial.file,'Rotation_external')
    task = 'TASK5';
    num = str2num(regexprep(temp,'Rotation_external ',''));
elseif contains(Trial.file,'Rotation_internal')
    task = 'TASK6';
    num = str2num(regexprep(temp,'Rotation_internal ',''));
elseif contains(Trial.file,'Static_reference1')
    task = 'STATIC1';
    num = str2num(regexprep(temp,'Static_reference1 ',''));
elseif contains(Trial.file,'Static_reference2')
    task = 'STATIC2';
    num = str2num(regexprep(temp,'Static_reference2 ',''));
elseif contains(Trial.file,'SBNNN')
    task = 'SBNNN';
    num = str2num(regexprep(temp,'SBNNN ',''));
elseif contains(Trial.file,'GBNNN')
    task = 'GBNNN';
    num = str2num(regexprep(temp,'GBNNN ',''));
elseif contains(Trial.file,'LBNNN')
    task = 'LBNNN';
    num = str2num(regexprep(temp,'LBNNN ',''));
elseif contains(Trial.file,'ABNNN')
    task = 'ABNNN';
    num = str2num(regexprep(temp,'ABNNN ',''));
elseif contains(Trial.file,'Gait')
    task = 'Gait';
    num = str2num(regexprep(temp,'Gait ',''));
elseif contains(Trial.file,'Static')
    task = 'Static';
    num = str2num(regexprep(temp,'Static ',''));
elseif contains(Trial.file,'SB')
    task = 'SB';
    num = str2num(regexprep(temp,'SB ',''));
elseif contains(Trial.file,'GB')
    task = 'GB';
    num = str2num(regexprep(temp,'GB ',''));
elseif contains(Trial.file,'LB')
    task = 'LB';
    num = str2num(regexprep(temp,'LB ',''));
elseif contains(Trial.file,'AB')
    task = 'AB';
    num = str2num(regexprep(temp,'AB ',''));
elseif contains(Trial.file,'BB')
    task = 'BB';
    num = str2num(regexprep(temp,'BB ',''));
elseif contains(Trial.file,'CB')
    task = 'CB';
    num = str2num(regexprep(temp,'CB ',''));
elseif contains(Trial.file,'DB')
    task = 'DB';
    num = str2num(regexprep(temp,'DB ',''));
elseif contains(Trial.file,'FB')
    task = 'FB';
    num = str2num(regexprep(temp,'FB ',''));
elseif contains(Trial.file,'TB')
    task = 'TB';
    num = str2num(regexprep(temp,'TB ',''));
elseif contains(Trial.file,'SINNN')
    task = 'SINNN';
    num = str2num(regexprep(temp,'SINNN ',''));
elseif contains(Trial.file,'GIKNN')
    task = 'GIKNN';
    num = str2num(regexprep(temp,'GIKNN ',''));
elseif contains(Trial.file,'UINNN')
    task = 'UINNN';
    num = str2num(regexprep(temp,'UINNN ',''));
elseif contains(Trial.file,'VILNN')
    task = 'VILNN';
    num = str2num(regexprep(temp,'VILNN ',''));
elseif contains(Trial.file,'VIRNN')
    task = 'VIRNN';
    num = str2num(regexprep(temp,'VIRNN ',''));
elseif contains(Trial.file,'XINNN')
    task = 'XINNN';
    num = str2num(regexprep(temp,'XINNN ',''));
end
if num < 10
    num = ['0',num2str(num)];
else
    num = num2str(num);
end

if rename_files
    newFile = [num2str(Patient_ID),'-',...
               Session_ID,'-',...
               Session_date,'-',...
               regexprep(Session_protocol,'KLAB-UPPERLIMB-',''),'-',...
               task,'-',...
               num,...
               '.c3d'];
else
    newFile = [temp,...
               '.c3d'];
end
cd(Folder.data);  
cd ..;
if ~isfolder('Processed')
    mkdir('Processed');
end
cd('Processed');
btkWriteAcquisition(btkFile,newFile);