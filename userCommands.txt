% -------------------------------------------------------------------------
% KLAB PRE-PROCESSING TOOLBOX
% USER COMMANDS FILE
% VERSION 3
% 08.08.2023
% AUTHOR: Florent Moissenet (modified by Gautier Grouvel)
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% CAUTION !!!!!!!!!!
% -------------------------------------------------------------------------
% First test the different methods on a small, representative data set,
% and visualize the output signals of each method to choose the most 
% suitable one.

% -------------------------------------------------------------------------
% SET UNITS
% -------------------------------------------------------------------------
% Set units used in the output C3D files
Units.output = 'mm';

% -------------------------------------------------------------------------
% C3D FILES
% -------------------------------------------------------------------------
staticTypes = {'SB'};
trialTypes  = {'GB'};

% -------------------------------------------------------------------------
% EXPORT C3D FILES
% -------------------------------------------------------------------------
% rename: Active (1) or Inactive (0)
rename_files = 0;

% -------------------------------------------------------------------------
% METADATA TO BE REMOVED
% -------------------------------------------------------------------------
MD_removed = { ...
};

MD_removed_sublist = { ...
};

% -------------------------------------------------------------------------
% EVENT
% -------------------------------------------------------------------------
% event:    Active (1) or Inactive (0)
% eventSet: Event name (one per line)
event = 0;
eventSet = { ...
    'Remote' ...
};

% -------------------------------------------------------------------------
% MARKER
% -------------------------------------------------------------------------
% marker:            Active (1) or Inactive (0)
% markerSet:         See following descriptions
% Column 1:          Marker name in c3d (before process)
% Column 2:          Output marker name (after process); Column 1 & 2 can have the same marker names
% Column 3:          Landmark type
%                    'landmark':        Marker related to a rigid body
%                    'semi-landmark':   Marker related to a curve
%                    'hybrid-landmark': Marker related to a curve and a rigid body
%                    'technical':       Marker not used for anatomical description
% Column 4:          Related rigid segment
%                    Use 'none' instead
% Column 5/6:        Related curve
%                    Only used with semi-landmark and hybrid-landmarks markers ('none' instead')
%                    Syntax: Curve named followed by order number on the curve 
% fmethod:           Fill gap the 3D trajectories
% fmethod.type:      Fill gap method
%                    'none':       No fill gap
%                    'linear':     Linear interpolation (1 point before and 1 point after gap are required)   
%                    'spline':     Cubic spline interpolation (10 points before and 10 points after gap are required)   
%                    'pchip':      Shape-preserving piecewise cubic interpolation (10 points before and 10 points after gap are required)
%                    'makima':     Modified Akima cubic Hermite interpolation (10 points before and 10 points after gap are required)   
%                    'intercor':   Marker trajectories intercorrelation (https://doi.org/10.1371/journal.pone.0152616)
%                    'rigid':      Apply rigid body transformation of the related segment on missing trajectories
%                                  (The missing trajectories must be part of a marker related to a rigid body)
%                                  (At least 3 other markers, without gap, are needed on each segment)
%                    'kalman':     Low dimensional Kalman smoothing (http://dx.doi.org/10.1016/j.jbiomech.2016.04.016)
%                                  NOT AVAILABLE YET
% fmethod.parameter: Fill gap parameter
%                    'none':       No parameter ([])
%                    'linear':     Maximum authorised gap width (number of frames)
%                    'spline':     Maximum authorised gap width (number of frames)  
%                    'pchip':      Maximum authorised gap width (number of frames) 
%                    'makima':     Maximum authorised gap width (number of frames)  
%                    'intercor':   No parameter ([])
%                    'rigid':      No parameter ([])
%                    'kalman':     No parameter ([])
% smethod.type:      Smoothing method
%                    'none':       No smoothing
%                    'butterLow2': Low pass filter (Butterworth 2nd order, [smethod.parameter] Hz)
%					 'butterLow4': Low pass filter (Butterworth 4th order, [smethod.parameter] Hz)
%                    'movmedian':  Moving median (window of [smethod.parameter] frames)
%                    'movmean':    Moving mean (window of [smethod.parameter] frames)
%                    'gaussian':   Gaussian-weighted moving average (window of [smethod.parameter] frames)
%                    'rloess':     Robust quadratic regression (window of [smethod.parameter] frames)
%                    'sgolay':     Savitzky-Golay filter (window of [smethod.parameter] frames)
% smethod.parameter: Smoothing parameter
%                    'none':       No parameter ([])
%                    'butterLow2': Cut-off frequency (Hz)
%					 'butterLow4': Cut-off frequency (Hz)
%                    'movmedian':  Window width (number of frames)
%                    'movmean':    Window width (number of frames)
%                    'gaussian':   Window width (number of frames)
%                    'rloess':     Window width (number of frames)
%                    'sgolay':     Window width (number of frames)
marker = 1;
markerSet = { ...
    'RFHD','RFHD','landmark','Head','none',nan; ...
    'RBHD','RBHD','landmark','Head','none',nan; ...
    'LBHD','LBHD','landmark','Head','none',nan; ...
    'LFHD','LFHD','landmark','Head','none',nan; ...
    'C7','C7','landmark','Thorax','none',nan; ...
    'CLAV','CLAV','landmark','Thorax','none',nan; ...
    'STRN','STRN','landmark','Thorax','none',nan; ...
    'T10','T10','landmark','Thorax','none',nan; ...
    'RBAK','RBAK','landmark','Thorax','none',nan; ...
    'LSHO','LSHO','landmark','LHumerus','none',nan; ...
	'LELB','LELB','landmark','LForearm','none',nan; ...
	'RSHO','RSHO','landmark','RHumerus','none',nan; ...
	'RELB','RELB','landmark','RForearm','none',nan; ...
	'LWRA','LWRA','landmark','LHand','none',nan; ...
	'LWRB','LWRB','landmark','LHand','none',nan; ...
	'LFIN','LFIN','landmark','LHand','none',nan; ...
	'RWRA','RWRA','landmark','RHand','none',nan; ...
	'RWRB','RWRB','landmark','RHand','none',nan; ...
	'RFIN','RFIN','landmark','RHand','none',nan; ...
    'LPSI','LPSI','landmark','Pelvis','none',nan; ...
    'LASI','LASI','landmark','Pelvis','none',nan; ...
    'RPSI','RPSI','landmark','Pelvis','none',nan; ...
    'RASI','RASI','landmark','Pelvis','none',nan; ...
	'LTHI','LTHI','landmark','LThigh','none',nan;...
	'LKNE','LKNE','landmark','LThigh','none',nan;...
	'RTHI','RTHI','landmark','RThigh','none',nan;...
	'RKNE','RKNE','landmark','RThigh','none',nan;...
	'LTIB','LTIB','landmark','LShank','none',nan;...
	'LANK','LANK','landmark','LShank','none',nan;...
	'RTIB','RTIB','landmark','RShank','none',nan;...
	'RANK','RANK','landmark','RShank','none',nan;...
	'LTOE','LTOE','landmark','LFoot','none',nan;...
	'LHEE','LHEE','landmark','LFoot','none',nan;...
	'RTOE','RTOE','landmark','RFoot','none',nan;...
	'RHEE','RHEE','landmark','RFoot','none',nan;...
};
Processing.Marker.fmethod.type      = 'intercor';
Processing.Marker.fmethod.parameter = [];
Processing.Marker.smethod.type      = 'butterLow4';
Processing.Marker.smethod.parameter = 6; 

% -------------------------------------------------------------------------
% EMG
% -------------------------------------------------------------------------
% emg:               Active (1) or Inactive (0)
% emgSet:            See following descriptions
% Column 1:          Channel number (CHi)
% Column 2:          Related EMG name
% zmethod.type:      Zeroing method
%                    'none':        No zeroing
%                    'mean':        Remove mean of the whole recording
%                    'frames':      Remove mean of a part of the recording
% zmethod.parameter: Zeroing parameter
%                    'none':        No parameter ([])
%                    'mean':        No parameter ([])
%                    'frames':      First and last frame of interest (e.g. [10 54])
% fmethod.type:      Filtering method
%                    'none':        No filtering
%                    'butterBand4': Band pass filter (Butterworth 4nd order, [fmethod.parameter fmethod.parameter] Hz)
% fmethod.parameter: Filtering parameter
%                    'none':        No parameter ([])
%                    'butterBand4': Minimum and maximum cut-off frequencies (Hz)  (e.g. [10 450])
% smethod.type:      Smoothing method
%                    'none':        No smoothing
%                    'butterLow2':  Low pass filter (Butterworth 2nd order, [smethod.parameter] Hz)
%                    'movmean':     Moving mean (window of [smethod.parameter] frames)
%                    'movmedian':   Moving median (window of [smethod.parameter] frames) 
%                    'rms':         Signal root mean square (RMS) (window of [smethod.parameter] frames)  
% smethod.parameter: Smoothing parameter
%                    'none':        No parameter ([])
%                    'butterLow2':  Cut-off frequency (Hz)
%                    'movmean':     Window width (number of frames)
%                    'movmedian':   Window width (number of frames)
%                    'rms':         Window width (number of frames)          
emg = 0;
emgSet = { ...
    'Channel_1','RDELTA'; ...
    'Channel_2','RDELTM'; ...
    'Channel_3','RDELTP'; ...
    'Channel_4','RTRAPS'; ...
    'Channel_5','RTRAPM'; ...
    'Channel_6','RSERRA'; ...
    'Channel_7','RLATD'; ...
    'Channel_8','LDELTA'; ...
    'Channel_9','LDELTM'; ...
    'Channel_10','LDELTP'; ...
    'Channel_11','LTRAPS'; ...
    'Channel_12','LTRAPM'; ...
    'Channel_13','LSERRA'; ...
    'Channel_14','LLATD' ...
};
Processing.EMG.zmethod.type      = 'mean';
Processing.EMG.zmethod.parameter = [];
Processing.EMG.fmethod.type      = 'butterBand4';
Processing.EMG.fmethod.parameter = [30 450]; % 30 Hz high pass filtering to remove potential heart rate artifact
Processing.EMG.smethod.type      = 'butterLow2';
Processing.EMG.smethod.parameter = 1.5;

% -------------------------------------------------------------------------
% Force
% -------------------------------------------------------------------------
% force:             Active (1) or Inactive (0)
% forceSet:          See following descriptions
% Column 1:          Channel number (CHi)
% Column 2:          Related force name
% zmethod.type:      Zeroing method
%                    'none':        No zeroing
%                    'mean':        Remove mean of the whole recording
%                    'frames':      Remove mean of a part of the recording
% zmethod.parameter: Zeroing parameter
%                    'none':        No parameter ([])
%                    'mean':        No parameter ([])
%                    'frames':      First and last frame of interest (e.g. [10 54])
% fmethod.type:      Filtering method
%                    'none':        No filtering
%                    'butterBand4': Band pass filter (Butterworth 4nd order, [fmethod.parameter fmethod.parameter] Hz)
% fmethod.parameter: Filtering parameter
%                    'none':        No parameter ([])
%                    'butterBand4': Minimum and maximum cut-off frequencies (Hz)  (e.g. [10 450])
% smethod.type:      Smoothing method
%                    'none':        No smoothing
%                    'butterLow2':  Low pass filter (Butterworth 2nd order, [smethod.parameter] Hz)
%                    'movmean':     Moving mean (window of [smethod.parameter] frames)
%                    'movmedian':   Moving median (window of [smethod.parameter] frames) 
%                    'rms':         Signal root mean square (RMS) (window of [smethod.parameter] frames)  
% smethod.parameter: Smoothing parameter
%                    'none':        No parameter ([])
%                    'butterLow2':  Cut-off frequency (Hz)
%                    'movmean':     Window width (number of frames)
%                    'movmedian':   Window width (number of frames)
%                    'rms':         Window width (number of frames)          
force = 0;
forceSet = { ...
    'Channel_16','FORCE' ...
};
Processing.Force.zmethod.type      = 'frames';
Processing.Force.zmethod.parameter = [10 50];
Processing.Force.fmethod.type      = 'butterBand4';
Processing.Force.fmethod.parameter = [10 450];
Processing.Force.smethod.type      = 'butterLow2';
Processing.Force.smethod.parameter = 10;

% -------------------------------------------------------------------------
% GRF
% -------------------------------------------------------------------------
% grf:               Active (1) or Inactive (0)
% grfSet:            Set the number of the forceplates for the ones you want to export data
% zmethod.type:      Zeroing method
%                    'none':        No zeroing
%                    'frames':      Remove mean of a part of the recording --> NOT AVAILABLE YET
%                    'offset':      Remove the offset defined during calibration
%                                   NOT AVAILABLE YET
% zmethod.parameter: Zeroing parameter
%                    'none':        No parameter ([])
%                    'frames':      First and last frame of interest (e.g. [10 54])
%                    'offset':      No parameter ([])
% fmethod.type:      Filtering method
%                    'none':        No filtering
%                    'threshold':   Vertical force threshold ([fmethod.parameter] N)
% fmethod.parameter: Filtering parameter
%                    'none':        No parameter ([])
%                    'threshold':   Force threshold (N)
% smethod.type:      Smoothing method
%                    'none':        No smoothing
%                    'butterLow2':  Low pass filter (Butterworth 2nd order, [smethod.parameter] Hz) 
% smethod.parameter: Smoothing parameter
%                    'none':        No parameter ([])
%                    'butterLow2':  Cut-off frequency (Hz)
% rmethod.type:      Resampling method
%                    'none':        No resampling
%                    'marker':      Resampled at marker trajectories sampling rate
% rmethod.parameter: Smoothing parameter
%                    'none':        No parameter ([])
%                    'marker':      No parameter ([])
grf = 1;
grfSet = [1,2,3];
grfCorrectFP = 0;
Processing.GRF.zmethod.type       = 'offset'; % If offset, requires GRF calibration data
Processing.GRF.zmethod.parameter  = [];
Processing.GRF.fmethod.type       = 'threshold';
Processing.GRF.fmethod.parameter  = 50;
Processing.GRF.smethod.type       = 'butterLow2';
Processing.GRF.smethod.parameter  = 50;
Processing.GRF.rmethod.type       = 'none';
Processing.GRF.rmethod.parameter  = [];